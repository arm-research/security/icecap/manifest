<!--
     Copyright 2018, Data61, CSIRO

     SPDX-License-Identifier: CC-BY-SA-4.0
-->

icecap-manifest
=================
The icecap project is a framework for building libraryOS, hypervisor, and vmm applications for seL4 using Rust.

This manifest is an attempt at enabling a seL4 style build system for IceCap.

For general instructions on using this repository, see [Getting Started](https://docs.sel4.systems/GettingStarted)
and the [IceCap page](https://gitlab.com/arm-research/security/icecap/icecap).

See [Host Dependencies](https://docs.sel4.systems/HostDependencies) for required dependencies.
